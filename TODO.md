# There are a lot of pending things to do

## Basic things to get started
~ List of words, I will take this from any good Pali dictionary and insert it into the database, I am using Yuttadhammo's at the moment
* Insert the List of Suttas in the database, I still don't have their full details, such as divisions and parents
* Insert a set of 'Clean' Sutas in the database, I will clean up the ones located in SuttaCentral
* Insert each word as it appears in the sutta in order to get adjacent words and distances and all that

I will probably start with a 'not so good' set of suttas, and improve on the code, as soon as possible I will reprocess all with the correct set of texts and dictionary

## Views

* Details of each sutta
* Words per sutta
* Words by frecuency
* Words distance
* Words proximity
