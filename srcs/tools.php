<?php

## Just a simple function to print things in the screen
function p($what){

	print $what . "\n";

}

## Just a simple function to print things in the screen, without a new line
function p2($what){

	print $what;

}

## Gets the full list of suttas from the DB
function getSuttaListing($code = false){

	global $db;

	$q = "SELECT * FROM suttas";

	if($code){
		$q .= " WHERE code like '" . $code . "%'";
	}

	$suttas = $db->query($q);

	$allSuttas = array();

	## Loop the suttas
	while($row = $suttas->fetch_assoc()){
		$allSuttas[] = $row;
	}

	$suttas->free();

	return $allSuttas;
}

