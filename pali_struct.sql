-- phpMyAdmin SQL Dump
-- version 4.2.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 22, 2014 at 07:41 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pali`
--

-- --------------------------------------------------------

--
-- Table structure for table `suttas`
--

CREATE TABLE IF NOT EXISTS `suttas` (
`idSutta` int(11) NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7204 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suttasTexts`
--

CREATE TABLE IF NOT EXISTS `suttasTexts` (
  `idSutta` int(11) NOT NULL,
  `pali` longtext COLLATE utf8_bin NOT NULL,
  `wordCount` int(11) NOT NULL,
  `average` int(11) NOT NULL,
  `longest` int(11) NOT NULL,
  `shortest` int(11) NOT NULL,
  `uniqueWords` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE IF NOT EXISTS `words` (
`wId` int(11) NOT NULL,
  `word` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=51233 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wordsAssoc`
--

CREATE TABLE IF NOT EXISTS `wordsAssoc` (
  `idWord` int(11) NOT NULL,
  `idSutta` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wordsInSuttas`
--

CREATE TABLE IF NOT EXISTS `wordsInSuttas` (
`pos` int(11) NOT NULL,
  `word` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `idSutta` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2765343 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `suttas`
--
ALTER TABLE `suttas`
 ADD PRIMARY KEY (`idSutta`);

--
-- Indexes for table `words`
--
ALTER TABLE `words`
 ADD PRIMARY KEY (`wId`);

--
-- Indexes for table `wordsInSuttas`
--
ALTER TABLE `wordsInSuttas`
 ADD PRIMARY KEY (`pos`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `suttas`
--
ALTER TABLE `suttas`
MODIFY `idSutta` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7204;
--
-- AUTO_INCREMENT for table `words`
--
ALTER TABLE `words`
MODIFY `wId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51233;
--
-- AUTO_INCREMENT for table `wordsInSuttas`
--
ALTER TABLE `wordsInSuttas`
MODIFY `pos` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2765345;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
