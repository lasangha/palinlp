<?php

include_once("../srcs/db.php");

$content = false;

## Word Distance
if(isset($_GET['calcDistance'])){

	$content .= "<h2>Word Distance</h2>";

	## Select word 1 and find out how many times it appears
	$q = sprintf("
		SELECT ws.*, s.code
		FROM wordsInSuttas as ws
		INNER JOIN suttas s ON s.idSutta = ws.idSutta
		WHERE ws.word like '%s'
		ORDER by ws.pos",
		$_GET['word_1']);

	$r = $db->query($q);

	## Select word 1 and find out how many times it appears
	$q2 = sprintf("
		SELECT ws.*, s.code
		FROM wordsInSuttas as ws
		INNER JOIN suttas s ON s.idSutta = ws.idSutta
		WHERE ws.word like '%s'
		ORDER by ws.pos",
		$_GET['word_2']);

	$r2 = $db->query($q2);

	## Total Ammount of occurences
	$content .= sprintf("I found <b><a href='?word=%s'>%s</a></b> %s times <br />", $_GET['word_1'], $_GET['word_1'], $r->num_rows);
	$content .= sprintf("I found <b><a href='?word=%s'>%s</a></b> %s times <br />", $_GET['word_2'], $_GET['word_2'], $r2->num_rows);

	## Store word's results in an array
	$word1 = array();
	$word2 = array();

	while($row2 = $r2->fetch_assoc()){
		$word2[] = $row2;
	}

	## An array with the information about distances and other variables
	$distances = array();

	// The minimum and maximum distance between words
	$minDistance = array('ini' => 0, 'end' => 0, 'total' => 10000000);
	$maxDistance = array('ini' => 0, 'end' => 0, 'total' => 0);
	$distanceA = array();
	$distance = 0;

	## Loop the results
	while($row = $r->fetch_assoc()){
		$word1[] = $row;
	}

	## Now I will do the actual analysis
	foreach($word1 as $w1){
		foreach($word2 as $w2){

			## Calculate the distance
			$distance = abs($w1['pos'] - $w2['pos']);
			$distanceA[] = $distance;

			## Update the minimum distance if this is less
			if($minDistance['total'] > $distance){
				$minDistance['total']    = $distance;
				$minDistance['ini']      = $w1['pos'];
				$minDistance['iniSutta'] = $w1['code'];
				$minDistance['end']      = $w2['pos'];
				$minDistance['endSutta'] = $w2['code'];
			}

			## Update the maximum distance if this is bigger
			if($maxDistance['total'] < $distance){
				$maxDistance['total']    = $distance;
				$maxDistance['ini']      = $w1['pos'];
				$maxDistance['iniSutta'] = $w1['code'];
				$maxDistance['end']      = $w2['pos'];
				$maxDistance['endSutta'] = $w2['code'];
			}
		}
	}

	## Report the information
	$content .= sprintf("The maximum distance is: %s | From: %s in <a href='?sutta=%s'>%s</a> To: %s in <a href='?sutta=%s'>%s</a> <br />",
						number_format($maxDistance['total']),
						number_format($maxDistance['ini']),
						$maxDistance['iniSutta'],
						$maxDistance['iniSutta'],
						number_format($maxDistance['end']),
						$maxDistance['endSutta'],
						$maxDistance['endSutta']);
	$content .= sprintf("The minimum distance is: %s | From: %s in <a href='?sutta=%s'>%s</a> To: %s in <a href='?sutta=%s'>%s</a> <br />",
						number_format($minDistance['total']),
						number_format($minDistance['ini']),
						$minDistance['iniSutta'],
						$minDistance['iniSutta'],
						number_format($minDistance['end']),
						$minDistance['endSutta'],
						$minDistance['endSutta']);
	$content .= sprintf("The average distance is: %s <br />", number_format(array_sum($distanceA) /  count($distanceA)));

}

# Get one word and its details
if(isset($_GET['word'])){

	$content .= "<h2>" . $_GET['word'] . "</h2>";

	$_GET['word'] = html_entity_decode($_GET['word']);

	$q = sprintf("
		SELECT wa.*, s.code, w.word
		FROM wordsAssoc as wa
		INNER JOIN suttas s ON s.idSutta = wa.idSutta
		INNER JOIN words w ON w.wId = wa.idWord
		WHERE w.word like '%s'
		ORDER by w.wId",
		$_GET['word']);

	$r = $db->query($q);

	$content .= "Found: " . $r->num_rows . " times <br />";

	## Loop the results
	while($row = $r->fetch_assoc()){
		$row['code'] = str_replace(".html", "", $row['code']);
		$content .= sprintf("<a href='?sutta=%s'>%s</a><br/>", $row['code'], $row['code']);
	}

}

# Get details about a sutta
if(isset($_GET['sutta'])){

	$content .= "<h2>" . $_GET['sutta'] . "</h2>";

	$_GET['sutta'] = html_entity_decode($_GET['sutta']);

	## Sutta Details
	$q = sprintf("
		SELECT st.*, s.code
		FROM suttasTexts st
		INNER JOIN suttas s ON s.idSutta = st.idSutta
		WHERE s.code = '%s'",
		$_GET['sutta']);

	$r = $db->query($q);

	$content .= "<h2>General Details</h2>";

	## Loop the results
	while($row = $r->fetch_assoc()){
		$content .= sprintf("<strong>Word Count</strong>: %s<br />", $row['wordCount']);
		$content .= sprintf("<strong>Average Word Size</strong>: %s<br />", $row['average']);
		$content .= sprintf("<strong>Longest Word</strong>: %s <br />", $row['longest']);
		$content .= sprintf("<strong>Shortest Word</strong>: %s<br />", $row['shortest']);
		$content .= sprintf("<strong>Unique Words</strong>: %s<br />", $row['uniqueWords']);
	}

	## Word Details
	$q = sprintf("
		SELECT wa.*, s.code, w.word
		FROM wordsAssoc as wa
		INNER JOIN suttas s ON s.idSutta = wa.idSutta
		INNER JOIN words w ON w.wId = wa.idWord
		WHERE s.code = '%s'
		ORDER by w.wId",
		$_GET['sutta']);

	$r = $db->query($q);

	## Loop the results
	while($row = $r->fetch_assoc()){
		$content .= sprintf("<a href='?word=%s'>%see</a> in %s<br/>", $row['word'], $row['word'], $row['code']);
	}

}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta charset="utf-8">
	<title>Pali Word Analisis (NLP)</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="jquery-ui/jquery-ui.css">
	<link rel="stylesheet" href="css.css">


    <script src="jquery-1.11.1.min.js"></script>
	<script src="jquery-ui/jquery-ui.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<style>
		.ui-autocomplete-loading {
			background: white url("jquery-ui/images/ui-anim_basic_16x16.gif") right center no-repeat;
		}
	</style>
<script>
$(function() {
	$(".words").autocomplete({
		source: "retriever.php?word=true",
		minLength: 2,
		select: function(event, ui) {}
	});
});

$(function() {
	$("#suttas").autocomplete({
		source: "retriever.php?sutta=true",
		minLength: 2,
		select: function(event, ui) {}
	});
});

</script>

</head>
<body>
<h1 class="page-header"><a href="index.php">Pali NLP (v0.1-alpha)</a></h1>

<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#word" aria-controls="word" role="tab" data-toggle="tab">View Word</a></li>
    <li role="presentation"><a href="#sutta" aria-controls="sutta" role="tab" data-toggle="tab">View Sutta</a></li>
    <li role="presentation"><a href="#compareWords" aria-controls="compareWords" role="tab" data-toggle="tab">Compare Words</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="word">
        <h3>View Word</h3>
        <div class="quickHelp">
            Type in a word, I will try to list all matching words I find and suggest options.
            Try searching for <strong>abbhācikkhi</strong>
        </div>
        <form class="form-inline">
            <div class="form-group">
                <input type="text" name="word" id="words" class="words form-control" placeholder="Word?">
                <input type="submit" value="View Word" class="btn btn-default">
            </div>
        </form>
    </div>
    <div role="tabpanel" class="tab-pane" id="sutta">
        <h3>View Sutta</h3>
        <div class="quickHelp">
            Type in the sutta number including its abreviations like MN100, I will try to list all matching options and give suggestions.
            Try searching for <strong>an10.100</strong>
        </div>
        <form class="form-inline">
            <div class="form-group">
                <input type="text" name="sutta" id="suttas" placeholder="Sutta?">
                <input type="submit" value="View Sutta">
            </div>
        </form>
    </div>
    <div role="tabpanel" class="tab-pane" id="compareWords">
        <h3>Compare Words</h3>
        <div class="quickHelp">
            Type in two words that you would like to compare, I will try to suggest matching words in each case.
            Try it comparing the words: <strong>abbhidā</strong> & <strong>abbhuggacchati</strong>
        </div>
        <form>
            <input type="hidden" value="1" name="calcDistance">
            <input type="text" name="word_1" id="word_1" class="words" placeholder="Word 1"><br />
            <input type="text" name="word_2" id="word_2" class="words" placeholder="Word 1"><br />
            <input type="submit" value="Calculate Distance">
        </form>
    </div>
  </div>

</div>

            <?php 
                if($content){
                    print "<div class='well' id='contentResults'>";
                    print $content;
                    print "</div>";
                }
            ?>

<table>
	<tr>
		<td valign="top" width="25%">

		</td>
		<td valign="top" style="padding: 10px; border-left: 1px solid white;">
		</td>
	</tr>
</table>

<div class="well">
    There are about 51232 words in the database at the moment.

    <h2>Tests you may perform</h2>
    <ul>
    <li>Try searching for <strong>abbhācikkhi</strong> in 'View Word'
    <li>Try searching for <strong>an10.100</strong> in 'View Sutta'
    <li>Compare the words: <strong>abbhidā</strong> & <strong>abbhuggacchati</strong>
    </ul>

    <h2>Pali NLP (sort of)</h2>
    Temporary vocabulary tests of the Pali Canon or NLP if you will. <br />
    This preview has tons! of errors, it is just a first attempt and there is a lot of work to be done. <br />
    <ul>
    <li>The main Pali Dictionary is not fully complete, I am currently using Bikkhu Yuttadhamo's Pali Dictionary.
    <li>The Suttas used need to be better cleaned from html code and other errors.
    </ul>
    <h2>It currently can:</h2>
    <ul>
    <li>Display stats of suttas such as: word count, longest word, shortest word, average word size, unique words.
    <li>Locate words in all suttas
    <li>Distances between 2 words: Maximum distance, minimum distance, 
    </ul>
    All work done here is Open Source and it will be released soon probably in gitorious.

    <h2>Todo:</h2>
    <ul>
    <li>List all adjacent words per each word
	<li>Get the correct names for each sutta
	<li>Get a clean sutta database, without html code or fix the cleaner I currently have
	<li>Create links in words found to the sutta where they are located and highlight occurences
    </ul>
</div>
</body>
</html>
