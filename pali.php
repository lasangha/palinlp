#!/usr/bin/env php

<?php

include_once("srcs/db.php");
include_once("srcs/tools.php");

## Create a list of unique words in the suttas
## This should not be a normal task and it can be a very hard task
//function countWords($ini = false){
## @deprecated ?
function listWordsInSuttas($ini = false){

	## Get the list of suttas
	p("Getting the sutta listing...");

	$suttaList = getSuttaListing($ini);

	## Get the word listing, it is big!
	p2("Getting the word listing... ");
	$wordList = getAllWords();
	p("Total words: " . count($wordList));

	p2("Working on suttas: ");

	foreach($suttaList as $suttaName){

		## Clean the name of extra chars
		$suttaName = trim($suttaName);

		//p("");
		p2("|" . str_replace(".html", "", $suttaName));

		if(file_exists("clean/" . $suttaName . ".txt")){

			## Read the file (sutta) into a string
			$thisSutta = file_get_contents("clean/" . $suttaName . ".txt");

            ## Loop each word and try to find it in the sutta
            ## @todo It might be better to loop the words in the sutta
			foreach($wordList as $w){
				if(trim($w) != ""){
					$w = trim($w);
					$count = substr_count($thisSutta, " " . $w . " ");

					if($count > 0){
						//p("Found word: " . $w . " " . $count . " times");
						$fp = fopen("words/" . $w, 'a+');
						fwrite($fp, str_replace(".html", "", $suttaName) . "|" . $count . "\n");
						fclose($fp);
					}
				}
			}
		}
		else{
			p("Error: File does not exist :(");
		}
	}
}

## Gets the full list of words in Pali from a dictionary, 
## @NOTWORKING!!!! I don't have an actuall dictionary I can use because the
## ones I have found are not based on the suttas I have, the way the words
## are constructed do not match, I am using just the words as they come
## from the suttas, however they are written and conjugated.
function getAllWords(){

	## Open the sutta list
	$wordList = file('stuff/paliDict');

	return $wordList;
}

## Insert the generated list of words in the DB, just a word list, no correlations
## This should come from an expected list of words like a dictionary, but for now
## I am using the words found in the suttas as they come.
## This should be a one time thing only. Repeat only if there are errors.
## @deprecated / @notInUse This is not been used at the moment use generateWordListing()
function insertWordListInDb(){

	global $db;

	## Get the list of suttas
	$wordList = getAllWords();

	foreach($wordList as $word){

		$word = trim($word);

		p("Word: " . $word);

		$q = sprintf("INSERT INTO words (word) VALUES('%s')", $word);

		$db->query($q);

	}

}

/******************
 * Set up and clean tasks
 *
 */

## Gets the full list of suttas from a text file, this is at the moment
## generated from the suttas downloaded from SuttaCentral with:
#  $ ls originalCanon >> suttaListing
function getBaseSuttaListing(){

	## Open the sutta list
	$suttaList = file('suttaList');

	return $suttaList;

}

## Gets the full list of suttas from the database, you may use $code in order to just filter by it eg 'AN'
## @deprecated ?
function d_getSuttaListing($code = false){

    global $db;

    $q = "
		SELECT st.*
		FROM suttas st
		";

	if($code){
		$q .= " WHERE code like '" . $code . "%'";
	}

	$suttas = $db->query($q);
	$allSuttas = array();

	## I need all the suttas in a big array
	while($rowS = $suttas->fetch_assoc()){
		$allSuttas[] = $rowS;
	}

	$q = sprintf("INSERT INTO suttas (code) VALUES('%s')", $suttaName);

    return $suttaList;

}

## Create a list of unique words in the suttas
## This should not be a normal task and it can be a very hard task
## This should be done once the suttas are clean
function generateWordListing($code = false){

	global $db;

	p("I will generate a list of unique words, wish me luck!");

	## Get the list of words in all the suttas
	p("Getting the suta listing...");

	$suttaList = getSuttaListing($code);

	if($suttaList){

		foreach($suttaList as $s){

			## Clean the name of extra chars
			$s = trim($s);

			p("Working on a sutta: " . $s);

			if(file_exists("clean/" . $s . ".txt")){

				$thisSutta = file_get_contents("clean/" . $s . ".txt");
				## Change new lines for spaces
				$thisSutta = str_replace("\n", " ", $thisSutta);

				## Split up in words
				$suttaWords = explode(" ", $thisSutta);

				foreach($suttaWords as $w){
					p2($w);
                    ## Select from db, if it exists, I will not create it again
                    #### Would it be better/faster to keep them in ram? a tmp file?
					$q = sprintf("SELECT * FROM words WHERE word = '%s'", $w);
					$r = $db->query($q);
					if($r->num_rows == 0){
						p("New word: $w\n ");
						$q = sprintf("INSERT INTO words (word) values('%s')", $w);
						$db->query($q);
					}
					else{
						echo "done\n";
					}
				}
			}
			else{
				p("Error: File does not exist :(");
			}
			exit();
		}
	}
}

## This will clean up the suttas as they where downloaded from SuttaCentral.net
## removing all html code, this should not be a common task, just once should be enough
## or if you find mistakes.
## I will create a new file with same name of the sutta + 'clean' in a new directory.
## Use $code to filter for just one code eg AN
function cleanUpSuttas($code = false){

	p("Cleaning up the suttas");

	$allSuttas = getSuttaListing($code);

	foreach($allSuttas as $suttaName){

		//$suttaName = trim($suttaName);
		$suttaName = $suttaName['code'];
		p2($suttaName);

		if(file_exists("originalCanon/" . $suttaName . ".html")){
			$a = file_get_contents("originalCanon/" . $suttaName . ".html");

			## Remove the header
			//preg_match('/msdiv" id="msdiv.*"><\/a>/', $a, $matches);
			preg_match('/<section class="sutta".*><article>/', $a, $matches);

/*
		## This gets funky, there are several markers to start
		if(count($matches) == 0){
			## This are used in it
			preg_match('/<a id="p_18It_.*" class="ms"><\/a>/', $a, $matches);
		}
 */
			if(count($matches) == 0){
				echo "\n\n\nERROR!!!!!!! PANIC!!!!! THE WORLD WILL END!!!!!!\n\n\n";
				echo $suttaName . "\n";

				exit();
			}

			## Locate the end of this header
			$p = strpos($a, $matches[0]) + strlen($matches[0]);
			$a2 = substr($a, $p);

			## Remove the footer
			$p = strpos($a2, "article>");
			$a2 = substr($a2, 0, ($p - 2));

			## Clean up the text from html and lower case it all
			$a2 = strtolower(strip_tags($a2));

			## Remove other stuff such as quotes and colons
			$a2 = str_replace(array('‘', '’', '?', ':', '“', '”', '—', '.', ';', ',', '…'), '', $a2);	

			## I will convert all new lines to spaces
			$a2 = str_replace("\n", " ", $a2);

			## Remove multiple spaces
			$a2 = preg_replace('!\s+!', ' ', $a2);

			## Removing soft hyphen which gives A LOT of problems when spliting words
			$a2 = preg_replace('/\xC2\xAD/', '', $a2);

			## Remove the sutta's name by finding the number that was there originally
			$numberPos = preg_match('/\d+/', $a2, $matchesTitle);

			# How many numbers did we find?
			$possibleTitles = count($matchesTitle) - 1;

			if($possibleTitles >= 0){
				# Now I can find the possition of that last title
				$titleSize = strpos($a2, $matchesTitle[$possibleTitles]);

				# Now I can remove all that part if it exists
				if($titleSize > 0){
					$a2 = substr($a2, $titleSize + 2);
				}
			}

			$fp = fopen("cleanCanon/" . $suttaName, 'w');
			fwrite($fp, $a2);
			fclose($fp);
		}
	}
}

## Insert the list of suttas in the database based in a text file with the expected full list of suttas.
## This should be a one time thing to do, repeated only if there are mistakes.
function insertSuttasCodesInDb(){

	global $db;

	## Get the list of suttas
	$suttaList = getBaseSuttaListing();

	foreach($suttaList as $suttaName){

		$suttaName = trim($suttaName);
		$suttaName = str_replace('.html', '', $suttaName);

		p("Sutta: " . $suttaName);

		$q = sprintf("INSERT INTO suttas (code) VALUES('%s')", $suttaName);

		$db->query($q);

	}

}

/******************
 * Analysis functions
 *
 */

## Insert the contents of the suttas in the database along with a list of details about this sutta,
## this is the basis for further analysis, which is usually just reading this information
function insertSuttasInDb($code = false){

	global $db;

	p("Inserting suttas texts in the db");

	## Get the list of suttas
	$suttaList = getSuttaListing($code);

	foreach($suttaList as $suttaName){

		p2($suttaName['code']);

		if(file_exists("cleanCanon/".$suttaName['code'])){

			$theSutta = file_get_contents("cleanCanon/" . $suttaName['code'], "r");

			## Get the words details about this sutta
			$suttaDetails = _suttaWordDetails($theSutta);

			$q = sprintf("INSERT INTO suttasTexts (idSutta, pali, wordCount, average, longest, shortest, uniqueWords)
				VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
					$suttaName['idSutta'],
					$theSutta,
					$suttaDetails['count'],
					$suttaDetails['average'],
					$suttaDetails['longest'],
					$suttaDetails['shortest'],
					$suttaDetails['uniqueWords']);

			$db->query($q);
			echo $db->error;
		}
		else{
			p("404");
		}

	}

}

## Get the size of the vocabulary

## Calculate the avarage word size in the sutta
## This is a helper function that you should not be calling directly
function _suttaWordDetails($text){

	$allWords = explode(" ", $text);

	$sizes = array();
	$longest = 0;
	$shortest = 1000;
	$count = 0;

	foreach($allWords as $word){
		$thisSize = strlen($word);
		$sizes[] = $thisSize;

		## Is this the longest?
		if($longest < $thisSize){
			$longest = $thisSize;
		}

		## Is this the shortest?
		if($shortest > $thisSize && $thisSize > 0){
			$shortest = $thisSize;
		}
	}

	## Get the distinct elements
	$uniqueWords = count(array_unique($allWords));

	## Get the avarage
	$average = array_sum($sizes) / count($sizes);

	return array('average' => $average, 'longest' => $longest, 'shortest' => $shortest, 'count' => count($allWords), 'uniqueWords' => $uniqueWords);

}


## This will count the words and insert them in the db
// function wordCounter($w = false, $s = false){

## This will create a list of words associations with suttas, meaning I will
## register each word as it appears in each sutta, much like a standard search
## engine would list word occurences in webpages.
## You may indicate a word sub set or a sutta subset for testing pourpuses,
## This should be a one time thing to do.
function wordAssocs($w = false, $s = false){

	global $db;

	## Select all suttas from the db
	p("Retrieving sutta listing");

	$q = "
		SELECT st.*
		FROM suttasTexts st
		INNER JOIN suttas s ON s.idSutta = st.idSutta
		";

	if($s){
		$q .= " WHERE code like '" . $s . "%'";
	}

	$suttas = $db->query($q);
	$allSuttas = array();

	## I need all the suttas in a big array
	while($rowS = $suttas->fetch_assoc()){
		$allSuttas[] = $rowS;
	}

	## Select all words from the db
	p("Retrieving words");

	$q = "SELECT * FROM words";

	if($w){
		$q .= " WHERE word like '" . $w . "%'";
	}

	$words = $db->query($q);

	p("Got all the information, now lets roll the loop");

	## Loop the words
	while($rowW = $words->fetch_assoc()){
		p("+");
		## Loop the suttas
		foreach($allSuttas as $thisSutta){
			$count = substr_count($thisSutta['pali'], " " . $rowW['word'] . " ");

			if($count > 0){
				p2($count . " ");
				$q = sprintf("INSERT INTO wordsAssoc (idWord, idSutta) values(%s, %s)", $rowW['wId'], $thisSutta['idSutta']);
				$db->query($q);
			}
		}
	}

	$words->free();
	$suttas->free();
}

## I will list all words in all the suttas and insert them in the database
## It is possible to just do this for some suttas deppending on their codes such as DN or MN
## @deprecated ? Not really deprecated, but what is this for? There is already a wordsAssoc table
## it is used by the web version in order to calculate certain things, it should be possible to use the other table mentioned
function insertAllWordsInSuttas($s =  false){

	global $db;

	p("Listing all words in suttas");

	## Get the list of suttas and texts
	$q = "
		SELECT st.*
		FROM suttasTexts st
		INNER JOIN suttas s ON s.idSutta = st.idSutta
		";

	if($s){
		$q .= " WHERE code like '" . $s . "%'";
	}

	$suttas = $db->query($q);

	//$allSuttas = array();

	while($row = $suttas->fetch_assoc()){

		## Split the text in words
		$allWords = explode(" ", $row['pali']);

		foreach($allWords as $word){
			$q = sprintf("INSERT INTO wordsInSuttas (word, idSutta) values('%s', '%s')", $word, $row['idSutta']);
			//p($q);
			$db->query($q);
			$db->error;
		}
	}

}

function helpPrint(){
    print "Heeeeeeelp!\n";
    print "Options are: \n\n";
    print " - h, help Get this help guide\n";
    print " - c, cleanUp Clean up the suttas from their html stuff, this should not be done a lot and it may take a WHILE. If this is a new install this should be the first step if you don't already have a clean set of suttas.\n";
}

p("Welcome");

## Things you can do ;)
## Just uncoment the line you want to execute, yes, I know I could do a menu, but not at the moment.
# $options = getopt("w::s::");

## Clean up tasks
# I will clean up all the suttas from their html parts, this should be done only once or whenever the suttas change
#cleanUpSuttas();

## Insert in DB
#insertSuttasCodesInDb();

#insertWordListInDb();

## Inserts the suttas in the database and performs an analysis on them
#insertSuttasInDb();

#generateWordListing();
#insertAllWordsInSuttas();

## What we came for!!! count and insert the words
#wordCounter('abb', false);

# The menu
$shortopts  = "";
$shortopts .= "h"; // These options do not accept values
$shortopts .= "c";
$shortopts .= "i";
$shortopts .= "d";
$shortopts .= "w";

$longopts  = array(
    "cleanUp",        // No value
    "help",           // No value
);

$options = getopt($shortopts, $longopts);

print_r($options);

if(array_key_exists("h", $options)){
    $task = "Help";
    helpPrint();
}elseif(array_key_exists("c", $options)){
    $task = "Clean up";
    //cleanUpSuttas();
}elseif(array_key_exists("i", $options)){
    $task = "Insert suttas in Database and perform analisys on them";
    //insertSuttasInDb();
}elseif(array_key_exists("d", $options)){
    $task = "Insert suttas codes in Database";
    //insertSuttasCodesInDb();
}elseif(array_key_exists("w", $options)){
    $task = "Insert all words in suttas in Database";
    //insertAllWordsInSuttas();
}

p("");
p("Done :)");


